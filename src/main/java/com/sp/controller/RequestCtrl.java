package com.sp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sp.model.Card;
import com.sp.model.CardFormDTO;
@Controller // AND NOT @RestController
public class RequestCtrl {
	
	
	@Autowired
	CardDao CardDao;
	
	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model) {
		return "index";
	}
	
	@RequestMapping(value = { "/view"}, method = RequestMethod.GET)
	public String view(Model model) {
		String test = new String();
		model.addAttribute("SearchForm", test);
		return "cardSearch";
	}
	
	@RequestMapping(value = { "/view"}, method = RequestMethod.POST)
	public String view(Model model, @RequestParam String SearchForm) {
		System.out.println(SearchForm);
		Card p=CardDao.getCardByName(SearchForm);
		model.addAttribute("myCard",p);
		return "cardView";
	}
	
	@RequestMapping(value = { "/list"}, method = RequestMethod.GET)
	public String viewList(Model model) {
		model.addAttribute("CardList",CardDao.getCardList() );
		return "cardViewList";
	}
	
	@RequestMapping(value = { "/addCard"}, method = RequestMethod.GET)
	public String addCard(Model model) {
		CardFormDTO CardForm = new CardFormDTO();
		model.addAttribute("CardForm", CardForm);
		return "cardForm";
	}
	
	
	@RequestMapping(value = { "/addCard"}, method = RequestMethod.POST)
	public String addCard(Model model, @ModelAttribute("CardForm") CardFormDTO CardForm) {
		Card p=CardDao.addCard(CardForm.getName(),CardForm.getDesc(),CardForm.getImgUrl(),CardForm.getFamily(),CardForm.getHp(),CardForm.getEnergy(),CardForm.getAttack(),CardForm.getDefence(),CardForm.getAffinity());
		model.addAttribute("myCard",p );
		return "cardView";
	}
	

}

