package com.sp.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.sp.model.Card;

@Service
public class CardDao {
	private List<Card> CardList;
	private Random randomGenerator;

	public CardDao() {
		CardList=new ArrayList<>();
		randomGenerator = new Random();
		createCardList();
	}

	private void createCardList() {
  
		Card p1=new Card("John", "pink", "http://ekladata.com/9-cPSlYvrenNHMVawFmf_gLx8Jw.gif","MylitlleCard",50,10,30,2,"Suicide");
		Card p2=new Card("Roberto", "blue", "http://ekladata.com/JEVyY9DkwX4vVkakeBfikSyPROA.gif","MylitlleCard",50,19,32,28,"Yolo");
		Card p3=new Card("Anna", "orange", "http://ekladata.com/fMJl--_v-3CmisaynTHju1DMeXE.gif","MylitlleCard",20,20,30,2,"Blue");
		Card p4=new Card("Angry Joe", "super angry power", "http://ekladata.com/AmbNNNvv-4YFEMZR8XD8e54WoHc.gif","MylitlleCard",0,20,30,29,"Lampadaire");
		Card p5=new Card("Ursula", "green", "http://ekladata.com/CXJhi2YLUbNz6__e0Ct6ZP-XOds.gif","MylitlleCard",10,10,30,2,"Banane");

		CardList.add(p1);
		CardList.add(p2);
		CardList.add(p3);
		CardList.add(p4);
		CardList.add(p5);
	}
	public List<Card> getCardList() {
		return this.CardList;
	}
	public Card getCardByName(String name){
		for (Card c : CardList) {
			if(c.getName().equals(name)){
				return c;
			}
		}
		return null;
	}
	public Card getRandomCard(){
		int index=randomGenerator.nextInt(this.CardList.size());
		return this.CardList.get(index);
	}

	public Card addCard(String name, String desc, String imgUrl, String family, int hp, int energy, int attack, int defence,String affinity) {
		Card p=new Card(name, desc, imgUrl,family, hp,energy,attack,defence,affinity);
		this.CardList.add(p);
		return p;
	}
}
