package com.sp.model;
public class CardFormDTO  {
	private String name;
	private String desc;
	private String imgUrl;
	private String family;
	private int Hp;
	private int Energy;
	private int Attack;
	private int Defence;
	private String affinity;

	public CardFormDTO() {
		this.name = "";
		this.desc = "";
		this.imgUrl="";
		this.family ="";
		this.Hp = 0;
		this.Energy = 0;
		this.Attack = 0;
		this.Defence = 0;
		this.affinity ="";
		
	}
	
	public CardFormDTO(String name, String desc, String imgUrl, String family, int hp, int energy, int attack, int defence, String affinity) {
		this.name = name;
		this.desc = desc;
		this.imgUrl = imgUrl;
		this.family = family;
		this.Hp = hp;
		this.Energy = energy;
		this.Attack = attack;
		this.Defence = defence;
		this.affinity =affinity;
	}
	
	public String getAffinity() {
		return affinity;
	}
	
	public void setAffinity(String affinity) {
		this.affinity = affinity;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getFamily() {
		return family;
	}

	public void setFamily(String family) {
		this.family = family;
	}

	public int getHp() {
		return Hp;
	}

	public void setHp(int hp) {
		Hp = hp;
	}

	public int getEnergy() {
		return Energy;
	}

	public void setEnergy(int energy) {
		Energy = energy;
	}

	public int getAttack() {
		return Attack;
	}

	public void setAttack(int attack) {
		Attack = attack;
	}

	public int getDefence() {
		return Defence;
	}

	public void setDefence(int defence) {
		Defence = defence;
	}

	

}